A text transformer for debugging normalization functions for the AbuseFilter MediaWiki extension.

Paste the text to transform (usually added_lines or new_wikitext) into the "Input", select the functions to apply, and the result will be written to the "Output". Then copy this to your regex engine of choice for further debugging.

https://mrpie5.gitlab.io/abusefilter-debugger/
